# README #

### What is this repository for? ###

* A simple Testcase
* All methods and info can be accessed via http://localhost:5555/home 
* Version 0.1

### Tech ###
* JSE8 (No JEE)
* EclispeLink as JPA Implementation
* Jersey and Embedded Jersey-Server
* Embedded Derby DB (others are supported too)

### How do I get set up? ###
* Summary of set up: *git clone https://bitbucket.org/edgarz/csv2db.git*
* Configuration: *application.properties and pom.xml*
* Dependencies: *see pom.xml*
* Database configuration: *in application.properties, check the existence of driver in pom.xml*
* How to run tests: on build
* Deployment instructions: none

### Who do I talk to? ###

* Repo owner or admin: ezwischenbrugger@gmail.com