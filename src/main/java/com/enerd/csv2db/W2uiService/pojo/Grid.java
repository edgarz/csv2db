package com.enerd.csv2db.W2uiService.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ez
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Grid {
    public String name;
    public String recid;
    public String url;
    public int limit = 50;
    public Map<String,Boolean> show = new HashMap<>();
    public List<Map> searches;
    public List<GridColumn> columns = new ArrayList<>();

    public Grid(String name) {
        this.name = name;
        this.url = "/data/"+name;
    }
}