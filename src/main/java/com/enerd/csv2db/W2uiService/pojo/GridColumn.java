package com.enerd.csv2db.W2uiService.pojo;

import eu.enerd.csv2db.app;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRawValue;
import javax.persistence.metamodel.SingularAttribute;

/**
 *
 * @author ez
 */
@JsonPropertyOrder({"field","caption","size","sortable","searchable","editable"})
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GridColumn {
    
    private transient Class clazz;
    private transient SingularAttribute sattr;
    
    
    public String field;
    public String caption;
    public String size = "100px";
    public boolean sortable = true;
    public boolean searchable = true;
    @JsonRawValue
    public String render = null;
    @JsonRawValue
    public String editable = null;
    
public GridColumn(Class clazz,SingularAttribute sattr) throws NoSuchFieldException {
    this.clazz = clazz;
    this.sattr = sattr;
    
    this.field =      sattr.getName();
    this.caption =    $prop("caption", sattr.getName());
    this.size =       $prop("size", "100px");
    this.sortable =   Boolean.valueOf($prop("sortable", "true"));
    this.searchable = Boolean.valueOf($prop("searchable", "false"));
    this.editable =   $prop("editable", null);
    this.render =     $prop("render", null);
    /*
    Field f = clazz.getDeclaredField(sattr.getName());
    Annotation annotation = f.getAnnotation(W2uiField.class);
    W2uiField w2uiField = (W2uiField) annotation;
    if(annotation!=null){
        if(!"".equals(w2uiField.caption())){
            this.caption = w2uiField.caption();
        }
        this.size = w2uiField.size();
        this.sortable = w2uiField.sortable();
        this.searchable = w2uiField.searchable();
        if(!"".equals(w2uiField.editable())){
            this.editable = w2uiField.editable();
        }
    }
    */        
}

private String $editable(String attr,String defaults){
    String key = "w2ui."+clazz.getSimpleName()+"."+sattr.getName()+"."+attr;
    String val = app.prop.getProperty(key, defaults);
    return val;
}

private String $prop(String attr,String defaults){
    String key = "w2ui."+clazz.getSimpleName()+"."+sattr.getName()+"."+attr;
    String val = app.prop.getProperty(key, defaults);
    return val;
}
}