package com.enerd.csv2db.service;

import eu.enerd.csv2db.W2uiService.W2uiFacade;
import eu.enerd.csv2db.W2uiService.W2uiException;
import com.enerd.csv2db.db.ParametrizedEntityManagerFactory;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;

/**
 * some services need classic interfaces, so here we have REST
 * @author ez
 * @param <T>
 */
public abstract class AbstractFacade<T> {
    private static final Logger logger = Logger.getLogger(AbstractFacade.class.getName());
    private final Class<T> entityClass;
    
    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }
    
    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        try {
            em.persist(entity);
            em.getTransaction().commit();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    public void edit(T entity) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        try {
            em.merge(entity);
            em.getTransaction().commit();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }
    
    public Field getPkField(EntityManager em) throws NoSuchFieldException{
        EntityType<T> et = em.getMetamodel().entity(entityClass);
        Field pkfield = entityClass.getDeclaredField(et.getId(null).getName());
        pkfield.setAccessible(true);
        return pkfield;
    }
    
    public void mergeAll(List<T> entities) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Set<Object> pkHash = new HashSet<>(entities.size());
        try {
            Field pkfield = getPkField(em);
            for(T entity:entities){ 
                Object pk = pkfield.get(entity);
                pkHash.add(pk);
            }
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<T>cq = builder.createQuery(entityClass);
            cq.select(cq.from(entityClass));
            List<T> dbEntities = em.createQuery(cq).getResultList();
            //delete outdated
            for(T entity:dbEntities){
                Object pk = pkfield.get(entity);
                if(!pkHash.contains(pk)){
                    em.remove(entity);
                }
            }
            //merge
            for(T entity:entities){
               em.merge(entity);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }    
    }

    public void remove(T entity) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        try {
            em.remove(em.merge(entity));
            em.getTransaction().commit();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    public T find(Object id) {
        EntityManager em = getEntityManager();
        T result = null;
        try {
            result = em.find(entityClass, id);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        } finally {
            em.close();
        }
        return result;
    }

    public List<T> findAll() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery<T>cq = em.getCriteriaBuilder().createQuery(entityClass);
            cq.select(cq.from(entityClass));
            return em.createQuery(cq).getResultList();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        } finally {
            em.close();
        }
        return null;
    }
    
    public int deleteAllEntities() {
        int cnt = 0;
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        try {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaDelete<T> query = builder.createCriteriaDelete(entityClass);
            query.from(entityClass);
            cnt = em.createQuery(query).executeUpdate();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return cnt;
    }

    public List<T> findRange(int[] range) throws W2uiException {
        List<T> result = null;
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<T> cq = cb.createQuery(entityClass);
            Root<T> root = cq.from(entityClass);
            cq.select(cq.from(entityClass));
            TypedQuery<T> q = getEntityManager().createQuery(cq)
                .setMaxResults(range[1] - range[0] + 1)
                .setFirstResult(range[0]);
            result = q.getResultList();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        } finally {
            em.close();
        }
        return result;
    }

    public Number count(String searchField,String searchOperator,String searchValue,String searchType) throws W2uiException {
        Number result = -1;
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<T> cq = cb.createQuery(entityClass);
            Root<T> root = cq.from(entityClass);
            CriteriaQuery<Long> cql = cb.createQuery(Long.class);
            cql.select(getEntityManager().getCriteriaBuilder().count(root));
            Query q = getEntityManager().createQuery(cql);
            result = ((Number) q.getSingleResult());
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        } finally {
            em.close();
        }
        return result;    
    }
    
}
