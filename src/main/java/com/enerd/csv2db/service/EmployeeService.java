package com.enerd.csv2db.service;

import eu.enerd.csv2db.Adapter.Employee.EmployeeAdapter;
import eu.enerd.csv2db.Adapter.Employee.Employees;
import com.enerd.csv2db.db.Employee;
import com.enerd.csv2db.db.ParametrizedEntityManagerFactory;
import java.text.ParseException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author ez
 */
@Path("/employee")
public class EmployeeService extends AbstractFacade<Employee> {

    public EmployeeService() {
        super(Employee.class);
    }
    
    /**
     * as we have no container, we have to get the EMF manually
     * @return 
     */
    @Override
    protected EntityManager getEntityManager() {
        EntityManagerFactory emf = ParametrizedEntityManagerFactory.createEntityManagerFactory();
        return emf.createEntityManager();
    }
    
    /**
     * Merge all Employees from CSV to Database.
     * New Records are persisted, existing ones are updated.
     * @return
     */
    @GET
    @Path("/merge")
    @Produces(MediaType.APPLICATION_JSON)
    public Response persistEmployeesFromFile() {
        Employees.mergeFromCsv();
        return Response.ok().build();
    }
    
    /**
     * start csv sync
     * @return 
     */
    @GET
    @Path("/startSync")
    //@Produces(MediaType.APPLICATION_JSON)
    public Response startSync() {
        Employees.startSync();
        return Response.ok("Scheduler started.").build();
    }
    
    /**
     * stop csv sync
     * @return 
     */
    @GET
    @Path("/stopSync")
    //@Produces(MediaType.APPLICATION_JSON)
    public Response stopSync() {
        Employees.stopSync();
        return Response.ok("Scheduler stopped.").build();
    }
    
    /**
     * Read CSV File, map the data to Entity Employee and list teh result.
     * @return
     * @throws ParseException 
     */
    @GET
    @Path("/phase1")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> getEmployeesFromFile() throws ParseException {
        return Employees.getEmployees(EmployeeAdapter.Mode.CSV);
    }
    
    /**
     * List active employees from DB.
     * @return 
     */
    @GET
    @Path("/phase2")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> getEmployeesFromDB() {
        return Employees.getEmployees(EmployeeAdapter.Mode.DB);
    }
    
    /**
     * List active employees from DB.
     * @return 
     */
    @GET
    @Path("/rest")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> getEmployeesFromREST() {
        return Employees.getEmployees(EmployeeAdapter.Mode.REST);
    }
}