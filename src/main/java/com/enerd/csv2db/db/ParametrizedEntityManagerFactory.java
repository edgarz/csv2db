package com.enerd.csv2db.db;

import eu.enerd.csv2db.app;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.spi.PersistenceUnitTransactionType;

/**
 * Singleton
 * @author ez
 */
public class ParametrizedEntityManagerFactory {
    private static final Logger logger = Logger.getLogger(ParametrizedEntityManagerFactory.class.getName());
            
    private static final String DATABASE = "com.enerd.csv2db.database";
    private static final String JDBC_DRIVER = "javax.persistence.jdbc.driver";
    private static final String JDBC_URL = "javax.persistence.jdbc.url";
    private static final String JDBC_USER = "javax.persistence.jdbc.user";
    private static final String JDBC_PASSWORD = "javax.persistence.jdbc.password";
    private static final String TRANSACTION_TYPE = "javax.persistence.transactionType";
    private static final String LOGGING_LEVEL = "eclipselink.logging.level";
    private static final String TARGET_SERVER = "eclipselink.target-server";
    private static final String TARGET_DB = "eclipselink.target-database";
    private static final String SCHEMAGEN_ACTION = "javax.persistence.schema-generation.database.action"; 
    
    private static EntityManagerFactory emf = null;
    
    @SuppressWarnings("unchecked")
    public static void init(){
        Map<String,String> properties = new HashMap<>();
        // Ensure RESOURCE_LOCAL transactions is used.
        properties.put(TRANSACTION_TYPE,PersistenceUnitTransactionType.RESOURCE_LOCAL.name());
        
        properties.put(TARGET_DB, app.prop.getProperty(TARGET_DB,"Derby"));
        String DB = properties.get(TARGET_DB)+".";
        // Configure the internal connection pool
        properties.put(JDBC_DRIVER, app.prop.getProperty(DB+JDBC_DRIVER,"org.apache.derby.jdbc.EmbeddedDriver"));
        properties.put(JDBC_URL, app.prop.getProperty(DB+JDBC_URL,"jdbc:derby:csv2db;create=true"));
        properties.put(JDBC_USER, app.prop.getProperty(DB+JDBC_USER,"APP"));
        properties.put(JDBC_PASSWORD, app.prop.getProperty(DB+JDBC_PASSWORD,"APP"));
        // Configure logging. FINE ensures all SQL is shown
        properties.put(LOGGING_LEVEL, app.prop.getProperty(LOGGING_LEVEL,"INFO"));
        // Ensure that no server-platform is configured
        properties.put(TARGET_SERVER, app.prop.getProperty(TARGET_SERVER,"NONE"));
        properties.put(SCHEMAGEN_ACTION, app.prop.getProperty(SCHEMAGEN_ACTION,"none"));
        emf = Persistence.createEntityManagerFactory("local",properties);
        //Persistence.createEntityManagerFactory("local");
        logger.log(Level.INFO, "EMF initialized.");
    }
    
    static {
        init();
    }
    
    public static EntityManagerFactory createEntityManagerFactory() {
        return emf;
    }
    
    private ParametrizedEntityManagerFactory(){};
}
