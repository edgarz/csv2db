/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enerd.csv2db.db;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * [Employee]: First Name, Last Name, Age, Full Address
 * @author ez
 */
@Entity
@Table(name="EMPLOYEE")
@JsonPropertyOrder({"id","firstName","lastName","age","fullAddress","active"})
public class Employee implements Serializable,Comparable {
    private static final long serialVersionUID = 1L;
    @Id
    private String  id;
    private String  firstName;
    private String  lastName;
    private int     age;
    private String  fullAddress;
    private boolean active;

    public Employee() {
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (toString() != null ? toString().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) object;
        if ((this.toString() == null && other.toString() != null) || (this.toString() != null && !this.toString().equals(other.toString()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return id+"..."+lastName+", "+firstName+" ["+age+"]";
    }
    
    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Employee)) {
            return -1;
        }
        Employee other = (Employee) o;
        return toString().compareTo(other.toString());
    }
}
