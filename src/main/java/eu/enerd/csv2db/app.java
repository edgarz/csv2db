package eu.enerd.csv2db;


import com.enerd.csv2db.db.ParametrizedEntityManagerFactory;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author ez
 */
public class app {
    public static final Properties prop = new Properties();

    public  static final Timer timer = new Timer();
    public  static HttpServer server = null;
    public static EntityManagerFactory emf;
    private static final Logger logger = Logger.getLogger(app.class.getName());
    
    static{
        try {
            init();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    public static void init() throws IOException, InterruptedException  {
        InputStream in = new FileInputStream("src/main/resources/application.properties");
        prop.load(in);
        in.close();
        Thread.sleep(100);
        emf = ParametrizedEntityManagerFactory.createEntityManagerFactory();
        Thread.sleep(100);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        try {
            String serverURI = app.prop.getProperty("app.serverURI");
            logger.log(Level.INFO,"Starting Embedded Jersey HTTPServer...\n");
            server = HttpServerFactory.create(serverURI);
            //todo server.setExecutor().
            server.start();
            logger.log(Level.INFO,"Jersey Application Server started Successfully with WADL available at {0}application.wadl.", serverURI);
            logger.log(Level.INFO,"WebClient available  at {0}home.", serverURI);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        } 
    }
    
    /**
     * wait max 1 seconds.
     */
    public static void kill(int delay) {
        logger.log(Level.INFO,"Stopping Embedded Jersey HTTPServer in {0} seconds!",delay);
        server.stop(delay);
        logger.log(Level.INFO,"Embedded Jersey HTTPServer stopped by user!");
    }
}
