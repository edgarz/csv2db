package eu.enerd.csv2db.Adapter.Employee;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Simple Pojo
 * @author ez
 */
@JsonPropertyOrder({"id","name","dateOfBirth","country","zip","city","street","isActive"})
public class EmployeeStage {
    public String  id;
    public String  name;
    public String  dateOfBirth;
    public String  street;
    public String  zip;
    public String  city;
    public String  country;
    public String  isActive;

    public EmployeeStage() {
    }
    
    public EmployeeStage(String id,String name,String dateOfBirth) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }

    public EmployeeStage setAddress(String country,String zip,String city,String street) {
        this.country = country;
        this.zip = zip;
        this.city = city;
        this.street = street;
        return this;
    }

    public EmployeeStage setIsActive(String isActive) {
        this.isActive = isActive;
        return this;
    }
}
