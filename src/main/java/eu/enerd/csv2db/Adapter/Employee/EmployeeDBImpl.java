package eu.enerd.csv2db.Adapter.Employee;

import com.enerd.csv2db.db.Employee;
import com.enerd.csv2db.db.ParametrizedEntityManagerFactory;
import com.enerd.csv2db.service.AbstractFacade;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class EmployeeDBImpl extends AbstractFacade<Employee> implements EmployeeInterface {

    public EmployeeDBImpl() {
        super(Employee.class);
    }
   
    @Override
    public List<Employee> getEmployees() {        
        return super.findAll();
    }    

    @Override
    protected EntityManager getEntityManager() {
        EntityManagerFactory emf = ParametrizedEntityManagerFactory.createEntityManagerFactory();
        return emf.createEntityManager();
    }
    
    /**
     * mergeAll all employees to DB.
     * @param employees 
     */
    @Override
    public void mergeAll(List<Employee> employees){
        super.mergeAll(employees);
    }
}  

