package eu.enerd.csv2db.Adapter.Employee;

import com.enerd.csv2db.db.Employee;
import java.util.List;
import java.util.stream.Collectors;

/**
 * v1.1 stream & filter implemented
 * @author ez
 */
public class EmployeeCSVImpl implements EmployeeInterface {
    EmployeeMapper mapper = new EmployeeMapper();
    AbstractCSVAdapter<EmployeeStage> employeeStageCSVAdapter  = new AbstractCSVAdapter<EmployeeStage>(EmployeeStage.class) {};      
            
    @Override
    public List<Employee> getEmployees() throws RuntimeException {   
        return employeeStageCSVAdapter
                .read()
                .stream()
                .filter(item -> "true".equals(item.isActive))
                .map(item -> mapper.from(item))
                .collect(Collectors.toList());
    }    
}  

