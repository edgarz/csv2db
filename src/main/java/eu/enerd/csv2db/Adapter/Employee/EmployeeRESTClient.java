package eu.enerd.csv2db.Adapter.Employee;

import eu.enerd.csv2db.app;
import com.enerd.csv2db.db.Employee;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;    
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;

/**
 * 
 * @author ez
 */
public class EmployeeRESTClient implements EmployeeInterface {
    private static final Logger logger = Logger.getLogger(EmployeeRESTClient.class.getName());
    public static final String CLIENT_URL = app.prop.getProperty("app.serverURI")+"employee/phase1";

    @Override
    public List<Employee> getEmployees() {
    Client client = Client.create();
    try {
        //get The converted CSV data by REST
        WebResource webResource = client.resource(CLIENT_URL);
        return webResource
            .accept(MediaType.APPLICATION_JSON)
            .get(new GenericType<List<Employee>>(){});
    } catch (ClientHandlerException | UniformInterfaceException ex) {
        logger.log(Level.SEVERE, null, ex);
    }
    return null;
}    
}  

