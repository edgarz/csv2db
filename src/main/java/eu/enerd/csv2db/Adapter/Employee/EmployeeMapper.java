package eu.enerd.csv2db.Adapter.Employee;

import eu.enerd.csv2db.app;
import com.enerd.csv2db.db.Employee;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 *
 * @author ez
 */
public class EmployeeMapper {

    public EmployeeMapper() {
    }
    
    /**
     * convert birthday to age (depends on Java 8 java.time)
     * @param birthDateString
     * @return
     * @throws ParseException 
     */
    private int getAge(String birthDateString) throws RuntimeException {
        try {
            SimpleDateFormat ddMMyyyy = new SimpleDateFormat(app.prop.getProperty("app.dateFormat"));
            Date birthDate = ddMMyyyy.parse(birthDateString);
            LocalDate bd = birthDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate now = (new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            Long years = ChronoUnit.YEARS.between(bd,now);
            return years.intValue();
        } catch (ParseException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * convert EmployeeStage to Employee
     * @param stage
     * @return 
     */
    public Employee from(EmployeeStage stage) throws RuntimeException {
        String[] nameParts = stage.name.trim().split(" ");
        boolean isReverse = stage.name.indexOf(',') > -1;
        Employee employee = new Employee();
        employee.setId(stage.id);
        if(nameParts.length != 2) {
            throw new RuntimeException("Name must consist of two parts, aktual lenght is "+nameParts.length);
        }
        employee.setId(stage.id);
        employee.setFirstName(nameParts[isReverse?1:0].trim());
        employee.setLastName(nameParts[isReverse?0:1].trim());
        employee.setAge(getAge(stage.dateOfBirth));
        employee.setFullAddress(stage.zip+" "+stage.city+", "+stage.street+", "+stage.country);
        employee.setActive("true".equals(stage.isActive));
        return employee;
    }

    public EmployeeStage from(Employee employee) throws Exception {
        throw new Exception ("EmployeeStage not implemented.");
    }
}