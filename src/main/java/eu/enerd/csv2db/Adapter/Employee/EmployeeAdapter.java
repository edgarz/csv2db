package eu.enerd.csv2db.Adapter.Employee;

import com.enerd.csv2db.db.Employee;
import java.util.List;

/**
 *
 * @author ez
 */
public class EmployeeAdapter implements EmployeeInterface {
    private final Mode mode;

    public EmployeeAdapter(Mode mode) {
        this.mode=mode;
    }
    
    @Override
    public List<Employee> getEmployees() {
        EmployeeInterface emp;
        switch(mode){
            case CSV:
                emp = new EmployeeCSVImpl();
                break;
            case REST:
                emp = new EmployeeRESTClient();
                break;
            case DB:
                emp = new EmployeeDBImpl();
                break;    
            default:    
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        return emp.getEmployees();
    }

    public enum Mode {
        CSV, REST, DB
    }
    
}
