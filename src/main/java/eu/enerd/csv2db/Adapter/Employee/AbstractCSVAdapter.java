package eu.enerd.csv2db.Adapter.Employee;

import eu.enerd.csv2db.app;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ez
 * @param <T>
 */
public abstract class AbstractCSVAdapter<T> {
    
    private final Class<T> entityClass;
    private final CsvMapper mapper = new CsvMapper();
    private final CsvSchema schema;
    private final File csvFile;
    
    /**
     * Initialize mapper, schema and Filename;
     * @param entityClass 
     * @return  
     */
    public AbstractCSVAdapter(Class<T> entityClass) {
        this.entityClass = entityClass;
        this.schema = mapper
                .schemaFor(entityClass)
                .withColumnSeparator(';')
                .withHeader();
        String tmpFile = app.prop.getProperty("app.csvFile");
        this.csvFile = new File(tmpFile);
    }
    
    /**
     * Write a csv file defined by class T
     * @param entities
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     * @throws IOException 
     */
    public void write(List<T>entities) throws FileNotFoundException, UnsupportedEncodingException, IOException{
        // output writer
        ObjectWriter myObjectWriter = mapper.writer(schema);
        myObjectWriter.writeValue(csvFile, entities);
    }
    
    /**
     * Read a csv file an map it to a list of class T.
     * @return
     * @throws IOException 
     */
    public List<T> read() throws RuntimeException{
        try {
            MappingIterator<T> it = mapper
                    .reader(EmployeeStage.class)
                    .with(schema)
                    .readValues(csvFile);
            List<T> entities = new ArrayList<>();
            while (it.hasNextValue()) {
                entities.add(it.nextValue());
            }
            return entities;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
}
