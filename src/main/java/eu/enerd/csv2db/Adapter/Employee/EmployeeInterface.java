package eu.enerd.csv2db.Adapter.Employee;

import com.enerd.csv2db.db.Employee;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author ez
 */
public interface EmployeeInterface {

    List<Employee> getEmployees() throws RuntimeException;
    
}
