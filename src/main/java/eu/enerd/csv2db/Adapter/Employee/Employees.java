package eu.enerd.csv2db.Adapter.Employee;

import eu.enerd.csv2db.app;
import com.enerd.csv2db.db.Employee;
import java.util.List;

/**
 *
 * @author ez
 */
public class Employees {
    private static EmployeeAdapter employeeAdapter;
    
    public static List<Employee> getEmployees(EmployeeAdapter.Mode mode){
        employeeAdapter = new EmployeeAdapter(mode);
        return employeeAdapter.getEmployees();
    }
    
    public static void mergeFromCsv(){
        employeeAdapter = new EmployeeAdapter(EmployeeAdapter.Mode.CSV);
        List<Employee> emloyees = employeeAdapter.getEmployees();
        EmployeeDBImpl empDb = new EmployeeDBImpl();
        empDb.mergeAll(emloyees);
    }
    
    public static void startSync(){
        final int syncDelay =  Integer.parseInt(app.prop.getProperty("com.enerd.csv2db.Employee.syncDelay", "10"));
        final int syncPeriod = Integer.parseInt(app.prop.getProperty("com.enerd.csv2db.Employee.syncPeriod", "60"));
        app.timer.schedule(new EmployeeCsvImportTask(), syncDelay,syncPeriod);
    }
    
    public static void stopSync(){
        app.timer.cancel();
    }

}
