package eu.enerd.csv2db.Adapter.Employee;

import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ez
 */
public class EmployeeCsvImportTask extends TimerTask {
    private static final Logger logger = Logger.getLogger(EmployeeCsvImportTask.class.getName());
    @Override
    public void run() {
        long t1 = System.currentTimeMillis();
        Employees.mergeFromCsv();
        long msec = System.currentTimeMillis()-t1; 
        logger.log(Level.INFO, "EmployeeCsvImportTask executed in {0} [msec].",msec);
    }
  }
