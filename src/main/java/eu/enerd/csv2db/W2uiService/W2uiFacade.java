package eu.enerd.csv2db.W2uiService;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

/**
 * some services need classic interfaces, so here we have REST
 * @author ez
 * @param <T>
 */
public abstract class W2uiFacade<T> {
    private static final Logger logger = Logger.getLogger(W2uiFacade.class.getName());
    private final Class<T> entityClass;
    private final EntityManagerFactory emf;
    private CriteriaBuilder cb;
    private CriteriaQuery<T> cq;
    private CriteriaQuery<Long> cql;
    private Root<T> root;
    
    protected LinkedHashMap<String,Object> payload = null;
    protected Integer limit  = 50;
    protected Integer offset = 0;
    protected String cmd     = "dummy";
    protected List<LinkedHashMap<String,Object>> sort = null;
    protected List<LinkedHashMap<String,Object>> search = null;
    protected List<String> selected = null;
    protected List<LinkedHashMap<String,Object>> changes = null;
    protected String sortField = null;
    protected String direction = null;
    protected String searchField = null;
    protected String searchOperator = null;
    protected Object searchValue = null;
    protected List<?> searchValues = null;
    protected String searchFieldType = null;
    protected int[] range; 

    public W2uiFacade(Class<T> entityClass,EntityManagerFactory emf) {
        this.entityClass = entityClass;
        this.emf = emf;
    }
    
    private EntityManager getEntityManager(){
        return emf.createEntityManager();
    }
    
    @SuppressWarnings("unchecked")
    protected void parsePayload(){
        limit  = (Integer) payload.get("limit");
        offset = (Integer) payload.get("offset");
        range = new int[]{offset,offset+limit-1};
        cmd     = (String)  payload.get("cmd");
        sort = (List<LinkedHashMap<String, Object>>) payload.get("sort");
        search = (List<LinkedHashMap<String,Object>>) payload.get("search");
        selected = (List<String>) payload.get("selected");
        changes = (List<LinkedHashMap<String,Object>>) payload.get("changes");
        if(sort != null && sort.size()>0){
           sortField = (String) sort.get(0).get("field");
           direction = (String) sort.get(0).get("direction");
        }
        if(search != null && search.size()>0){
           searchField = (String) search.get(0).get("field");
           searchOperator = (String) search.get(0).get("operator");
           searchValue = search.get(0).get("value");
           if(search.get(0).get("value").getClass().isAssignableFrom(ArrayList.class)){
                searchValues = (List<?>) searchValue;
           } 
           searchFieldType = (String) search.get(0).get("type");
        }
    }
    
    @SuppressWarnings("unchecked")
    private void mergeAll(EntityManager em) throws Exception {
        if(changes==null || changes.size()==0){
            throw new W2uiException("No data selected!");
        }
        ObjectMapper objectMapper = new ObjectMapper();
        EntityType<T> entityType = em.getMetamodel().entity(entityClass);
        String pkFieldName = entityType.getId(null).getName();
        for(LinkedHashMap<String,?> change:changes){
            Object recid = change.get("recid");
            T entity = em.find(entityClass, recid);
            change.remove("recid");
            for(String key:change.keySet()){
                BeanWrapper wrapper = new BeanWrapperImpl(entity); 
                wrapper.setPropertyValues(change);
                entity = (T)wrapper.getWrappedInstance();
                em.merge(entity);
            }
        }
    }
    
    private void delete(EntityManager em) throws Exception {
        if(selected==null || selected.isEmpty()){
            throw new W2uiException("No data selected!");
        }
        ObjectMapper objectMapper = new ObjectMapper();
        EntityType<T> entityType = em.getMetamodel().entity(entityClass);
        String pkFieldName = entityType.getId(null).getName();
        for(String recid:selected){
            T entity = em.find(entityClass, recid);
            em.remove(entity);
        }
    }

    private void buildCriteria() throws W2uiException{
        if(null != searchOperator) {
            Path<String> path = root.get(searchField);
            switch (searchOperator) {
            case "begins":
                cq.where(cb.like(path, searchValue+"%"));
                cql.where(cb.like(path, searchValue+"%"));
                break;
            case "contains":
                cq.where(cb.like(path, "%"+searchValue+"%"));
                cql.where(cb.like(path, "%"+searchValue+"%"));
                break;
            case "is":
                cq.where(cb.equal(path, searchValue));
                cql.where(cb.equal(path, searchValue));
                break;
            case "between":
                if("int".equals(searchFieldType)) {
                    cq.where(cb.between(path, searchValues.get(0).toString(),searchValues.get(1).toString()));
                    cql.where(cb.between(path, searchValues.get(0).toString(),searchValues.get(1).toString()));
                } else {
                    throw new W2uiException("Operator '"+searchOperator+"' not implemented yet!");
                }
                break;    
            default:
                throw new W2uiException("Operator '"+searchOperator+"' not implemented yet!");
            }
        }
        if(sortField != null){
            List<Order> sortList = new ArrayList<>();
            Path<String> path = root.get(sortField);
            sortList.add("asc".equals(direction)?cb.asc(path):cb.desc(path));
            cq.orderBy(sortList);
        }
    }

    private W2uiResult<T> findRange(EntityManager em,W2uiResult<T> result) throws Exception {
        cb = em.getCriteriaBuilder();
        cq = cb.createQuery(entityClass);
        cql = cb.createQuery(Long.class);
        root = cq.from(entityClass);
        buildCriteria();
        TypedQuery<T> q = em.createQuery(cq)
                .setMaxResults(range[1] - range[0] + 1)
                .setFirstResult(range[0]);
        q.setHint("eclipselink.read-only"/*QueryHints.READ_ONLY*/, "True"/*HintValues.TRUE*/); 
        result.setRecords(q.getResultList());
        int resultSize = result.getRecords().size();
        if(resultSize < limit){
            result.setTotal(offset+resultSize);
        } else {
            cql.select(em.getCriteriaBuilder().count(root));
            TypedQuery<Long> qcnt = em.createQuery(cql);
            result.setTotal(qcnt.getSingleResult());
        }
        return result;
    }
    
    public W2uiResult<T> post(LinkedHashMap<String,Object> payload) {
        long t1 = System.currentTimeMillis();
        EntityManager em = getEntityManager();
        W2uiResult<T> result = new W2uiResult<T>(){private static final long serialVersionUID = 1L;
        };
        this.payload = payload;
        try {
            parsePayload();
            em.getTransaction().begin();
            switch(cmd){
                case "save-records":
                    mergeAll(em);
                    em.getTransaction().commit();
                    break;
                case "delete-records":
                    delete(em);
                    em.getTransaction().commit();
                    break;
                default:
                    result = findRange(em,result);
            }  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
            result.setStatus("error");
            result.setMessage(ex.getMessage());
        } finally {
            em.close();
        }
        logger.log(Level.INFO, "Query took {0}[msec]", (System.currentTimeMillis()-t1));
        return result;
    };
}
