package eu.enerd.csv2db.W2uiService;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 * Setup Jersey aka jax-rs
 * @author ez
 */
@javax.ws.rs.ApplicationPath("/")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
    }
    
    
    
}
