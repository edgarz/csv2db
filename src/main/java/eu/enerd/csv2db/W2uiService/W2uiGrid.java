package eu.enerd.csv2db.W2uiService;

import com.enerd.csv2db.W2uiService.pojo.Grid;
import com.enerd.csv2db.W2uiService.pojo.GridColumn;
import eu.enerd.csv2db.app;
import com.enerd.csv2db.db.ParametrizedEntityManagerFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author ez
 */
@Path("/grid")
public class W2uiGrid {

@GET
@Path("/{entity}")
@Produces(MediaType.APPLICATION_JSON)
public String W2uiGrid(@PathParam("entity") String EntityName) throws ClassNotFoundException, JsonProcessingException, NoSuchFieldException  {
    EntityManager em = app.emf.createEntityManager();
    Class clazz = Class.forName("com.enerd.csv2db.db."+EntityName);
    EntityType entityType = em.getMetamodel().entity(clazz);
    String pkFieldName = entityType.getId(null).getName();
    
    Grid grid = new Grid(EntityName);
    grid.recid = pkFieldName;
    grid.show.put("toolbar", Boolean.TRUE);
    grid.show.put("toolbarAdd", Boolean.TRUE);
    grid.show.put("toolbarDelete", Boolean.TRUE);
    grid.show.put("toolbarSave", Boolean.TRUE);
    grid.show.put("footer", Boolean.TRUE);
    //todo colum order
    for(Object attr:entityType.getSingularAttributes()){
            GridColumn col = new GridColumn(clazz, (SingularAttribute) attr);
            grid.columns.add(col);
    }
    return new ObjectMapper()
        .writer()
        .withDefaultPrettyPrinter()
        .writeValueAsString(grid);
}    

}
