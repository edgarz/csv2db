package eu.enerd.csv2db.W2uiService;

import com.enerd.csv2db.db.ParametrizedEntityManagerFactory;
import java.util.LinkedHashMap;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author ez
 */
@Path("/data")
public class W2uiREST {
private static final EntityManagerFactory emf = ParametrizedEntityManagerFactory.createEntityManagerFactory();

@POST
@Path("/{entity}")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public Response post(@PathParam("entity") String EntityName,LinkedHashMap<String,Object> payload) {
        try {
            Class clazz = Class.forName("com.enerd.csv2db.db."+EntityName);
            W2uiFacade wgf = new W2uiFacade(clazz,emf) {};
            return Response.ok(wgf.post(payload)).build();
        } catch (ClassNotFoundException ex) {
            return Response.noContent().build();
        }
};



}
