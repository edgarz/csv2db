package eu.enerd.csv2db.W2uiService;

/**
 *
 * @author ez
 */
public class W2uiException extends Exception {
    public W2uiException() { super(); }
    public W2uiException(String message) { super(message); }
    public W2uiException(String message, Throwable cause) { super(message, cause); }
    public W2uiException(Throwable cause) { super(cause); }
}
