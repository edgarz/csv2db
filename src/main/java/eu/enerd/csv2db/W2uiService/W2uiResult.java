package eu.enerd.csv2db.W2uiService;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author ez
 * @param <T>
 */
public abstract class W2uiResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private String  status = "success";
    private Number  total;
    private String  message;
    private List<T> records;

    public W2uiResult() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Number getTotal() {
        return total;
    }

    public void setTotal(Number total) {
        this.total = total;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }
    
    
}
