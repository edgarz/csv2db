package eu.enerd.csv2db.W2uiService;

import java.io.File;
import java.text.ParseException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author ez
 */
@Path("/home")
public class FileService {


@GET
@Produces(MediaType.TEXT_HTML)
public Response index(@PathParam("file") String file) throws ParseException {
    File fileToSend = new File("src/main/resources/W2uiClient/index.html");
    return Response.ok(fileToSend,MediaType.TEXT_HTML_TYPE).build();
}

@GET
@Path("/{file}")
public Response getFile(@PathParam("file") String file) throws ParseException {
    File fileToSend = new File("src/main/resources/W2uiClient/"+file);
    return Response.ok(fileToSend).build();
}
}
