package eu.enerd.csv2db.W2uiService;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ez
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface W2uiField {
    String  caption() default "";
    String  size() default "100px";
    boolean sortable() default true;
    boolean searchable() default true;
    String editable() default "";
    String render() default "";
}
