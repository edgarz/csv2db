package com.enerd.csv2db.Employees;

import eu.enerd.csv2db.Adapter.Employee.EmployeeAdapter;
import eu.enerd.csv2db.Adapter.Employee.EmployeeDBImpl;
import eu.enerd.csv2db.Adapter.Employee.Employees;
import eu.enerd.csv2db.app;
import com.enerd.csv2db.db.Employee;
import eu.enerd.csv2db.Adapter.Employee.AbstractCSVAdapter;
import eu.enerd.csv2db.Adapter.Employee.EmployeeStage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author ez
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EmployeesTest {
    private static Employee[] csvArray;
    private static List<EmployeeStage> employees = new ArrayList<>();
    private static final Logger logger = Logger.getLogger(EmployeesTest.class.getName());
    private static final AbstractCSVAdapter<EmployeeStage> employeeStageCSVAdapter  = new AbstractCSVAdapter<EmployeeStage>(EmployeeStage.class) {};
    public static int CNT;
    
    public EmployeesTest() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
        
    }
    
    /**
     * Test of getEmployees method, of class Employees.
     */
    @Test 
    public void test0Write(){
        CNT = Integer.parseInt(app.prop.getProperty("com.enerd.csv2db.csv.CNT","1000"));
        try {
            employees = new ArrayList<>();
            String[] fnames = {"Andrea","Hans","Max","Toni","Luke"};
            String[] lnames = {"Moser","Skywalker","Mustermann","Feimann","Jobs"};
            String[] district = {"1010","1070","1160","1020","1200"};
            EmployeeStage ed = new EmployeeStage("ED","Edgar Zwischenbugger","01.05.1965")
                    .setAddress("Austria","1070","Vienna","Halbgasse 11/21")
                    .setIsActive("false");
            employees.add(ed);
            EmployeeStage be = new EmployeeStage("BE","Benjamin Schwaerzler","01.01.1985")
                    .setAddress("Austria","1010","Vienna","Rotenturmstraße 27/14")
                    .setIsActive("true");
            employees.add(be);
            for(int i=1; i < CNT;i++){
                int rand = ((Double)Math.floor((Math.random() * CNT) + 1)).intValue();
                EmployeeStage jd = new EmployeeStage("JD"+i
                        ,fnames[rand%234%fnames.length]+" "+lnames[rand%274%lnames.length]
                        ,"01.0"+1+i%9+"."+(1960+rand%34%40))
                        .setAddress("Austria"
                                ,district[rand%274%district.length]
                                ,"Vienna"
                                ,"Halbgasse "+rand%999)
                        .setIsActive(rand%2==0?"false":"true");
                employees.add(jd);
            }
            employeeStageCSVAdapter.write(employees);
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void test1CsvEmployees() {
        List<Employee> list =  Employees.getEmployees(EmployeeAdapter.Mode.CSV);
        System.out.println("CSV Adapter: "+list.size()+" Employees received.");
        assertNotNull(list);
        csvArray = list.toArray(new Employee[list.size()]);
        Arrays.sort(csvArray);
    }
    
    @Test
    public void test2DBEmployees() {
        EmployeeDBImpl employeeDBImpl = new EmployeeDBImpl();
        employeeDBImpl.mergeAll(Arrays.asList(csvArray));
        List<Employee> list =   Employees.getEmployees(EmployeeAdapter.Mode.DB);
        System.out.println("DB Adapter: "+(list==null?0:list.size())+" Employees received.");
        assertNotNull(list);
        Employee[] dbArray = list.toArray(new Employee[list.size()]);
        Arrays.sort(dbArray);
        assertArrayEquals(csvArray,dbArray);
    }
    
    @Test
    public void test3RESTEmployees() {
    try {    
        app.main(new String[]{});
        List<Employee> list = Employees.getEmployees(EmployeeAdapter.Mode.REST);
        System.out.println("RESTfull Adapter: "+list.size()+" Employees received.");
        assertNotNull(list);
        Employee[] restArray = list.toArray(new Employee[list.size()]);
        Arrays.sort(restArray);
        assertArrayEquals(csvArray,restArray);
    } catch (Exception ex) {
        fail(ex.getMessage());
    } finally { 
        if(app.server != null){
            app.kill(1);
        }
    }    
    }
}
